import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
import { AgGridModule } from 'ag-grid-angular';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { DocumentsComponent } from './documents/documents.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { FleetComponent } from './fleet/fleet.component';
import { LeasesComponent } from './leases/leases.component';
import { TasksComponent } from './tasks/tasks.component';
import { ClientsComponent } from './clients/clients.component';
import { FleetFormComponent } from './fleet/fleet-form/fleet-form.component';
import { ForumSummaryComponent } from './forum/forum-summary/forum-summary.component';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ForumPostComponent } from './forum/forum-post/forum-post.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    DashboardComponent,
    UsersComponent,
    UsersFormComponent,
    DocumentsComponent,
    FleetComponent,
    LeasesComponent,
    TasksComponent,
    ClientsComponent,
    FleetFormComponent,
    ForumSummaryComponent,
    ForumPostComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ChartsModule,
    AgGridModule.withComponents(),
    PdfViewerModule,
    NgxExtendedPdfViewerModule,
    NgbModule
  ],
  providers: [
    NgbActiveModal
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
