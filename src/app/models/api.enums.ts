export enum StandardErrors {
  network = "Network error.",
  form = "Incorrext form data."
}

export enum UserRole {
    "Administrator",
    "Director",
    "Office",
    "Driver",
    "Broker"
}

export enum CarStatus{
  "Ghost",
  "Avilable",
  "In use",
  "In service",
  "Withdrawn"
}

export enum LeaseStatus{
  "Open",
  "Started",
  "Finished",
  "Cancelled"
}

export enum TaskType {
  "hand car",
  "pick car",
  "inspection",
  "relocation"
}

export enum TaskStatus{
  "open",
  "finished",
  "cancelled"
}

export enum DocumentType {
  "Invoice",
  "Let out protocol",
  "Take back protocol"
}

export enum PostCategories {
  "Alert",
  "Info"
}
