import { PostCategories } from './api.enums';

export class Message {
  message: string;
}

export class User {
    id?: number;
    name: string;
    login: string;
    password: string;
    email: string;
    role: number;
    department: string;
}

export class Car {
    id?: number;
    department: string;
    registrationNumber: string;
    brand: string;
    model: string;
    acriss: string;
    tires: string;
    vin: string;
    productionYear: number;
    status: number;
}

export class Lease {
  id?: number;
  clientId: number;
  carId: number;
  startDate: string;
  endDate: string;
  status: number;
  department: string;
}

export class Task {
  id?: number;
  leaseId: number;
  userId: number;
  type: number;
  status: number;
  address: string;
  comments: string;
}

export class Client {
  id?: number;
  name: string;
  surname: string;
  age: number;
  pesel: string;
  address: string;
  email: string;
  phone: number;
}

export class Document {
  id?: number;
  leaseId: number;
  type: number;
  file?: Blob;
}

export class Post {
  id?: number;
  authorId: number;
  title: string;
  category: PostCategories;
  message: string;
}
