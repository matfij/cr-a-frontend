import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { AgGridAngular } from 'ag-grid-angular';
import { StandardErrors, DocumentType } from '../models/api.enums';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {

  message: string = null;

  fileUrl = null;

  // grid properties
  columnDefs = [
    { headerName: 'ID', field: 'id', sortable: true, filter: true, checkboxSelection: true },
    { headerName: 'Lease ID', field: 'leaseId', sortable: true, filter: true },
    { headerName: 'Type', field: 'type', sortable: true, filter: true }
  ];

  rowData = Array();

  @ViewChild('usersGrid', { static: false })
  usersGrid: AgGridAngular;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.getDocumentsInfo().subscribe(documents => {
      const docData = [];
      documents.forEach(doc => {
        docData.push({
          'id': doc.id,
          'leaseId': doc.leaseId,
          'type': DocumentType[doc.type],
        })
        this.rowData = docData;
      }, err => {
        this.message = err;
      })
    });
  }

  preview() {
    var selectedRows = this.usersGrid.api.getSelectedRows();
    var document = selectedRows[0];

    this.apiService.getDocumentDetails(document.id).subscribe(file => {
      this.fileUrl = file;
    }, err => {
      this.message = StandardErrors.network;
    });
  }

  download() {

  }

  hide() {
    this.fileUrl = null;
  }

}
