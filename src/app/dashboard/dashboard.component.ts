import { Component, OnInit } from '@angular/core';
import { ChartOptions , ChartDataSets, ChartType, RadialChartOptions } from 'chart.js'
import { Label, Color, SingleDataSet, MultiDataSet } from 'ng2-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  // leases chart
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Assistance' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Broker' }
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:  {
    responsive: true
  };
  public lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    {
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(255,12,12,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartType = 'line';
  public lineChartLegend = true;

  // fleet chart
  public polarAreaChartLabels: Label[] = ['BWM', 'Audi', 'Fiat', 'Tesla', 'Nissan'];
  public polarAreaChartData: SingleDataSet = [300, 200, 100, 40, 120];
  public polarAreaLegend = true;
  public polarAreaChartType: ChartType = 'polarArea';


  constructor() { }

  ngOnInit(): void {
  }
}
