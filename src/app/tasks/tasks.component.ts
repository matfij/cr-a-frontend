import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { LeaseStatus, TaskStatus, TaskType } from '../models/api.enums';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  message: string = null

  // grid properties
  columnDefs = [
    { headerName: 'ID', field: 'id', sortable: true, filter: true, checkboxSelection: true },
    { headerName: 'Lease ID', field: 'leaseId', sortable: true, filter: true },
    { headerName: 'User ID', field: 'userId', sortable: true, filter: true },
    { headerName: 'Type', field: 'type', sortable: true, filter: true },
    { headerName: 'Status', field: 'status', sortable: true, filter: true },
    { headerName: 'Address', field: 'address', sortable: true, filter: true },
    { headerName: 'Comments', field: 'comments', sortable: true, filter: true }
  ];

  rowData = Array();

  @ViewChild('fleetGrid', { static: false })
  fleetGrid: AgGridAngular;

  constructor(
    private apiService: ApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.apiService.getAllTasks().subscribe(tasks => {
      const taskData = [];
      tasks.forEach(task => {
        taskData.push({
          'id': task.id,
          'leaseId': task.leaseId,
          'userId': task.userId,
          'type': TaskType[task.type],
          'status': TaskStatus[task.status],
          'address': task.address,
          'comments': task.comments,
        })
        this.rowData = taskData;
      }, err => {
        this.message = err;
      })
    });
  }

  displaySelected() {
    var selectedRows = this.fleetGrid.api.getSelectedRows();
    var user = selectedRows[0];

    // this.router.navigate(['main/users-form', user.id]);
  }

  createNew() {
    // this.router.navigate(['main/users-form', 0]);
  }

}
