import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User, Message, Car, Lease, Task, Document, Client, Post } from './models/api.models';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private _baseUrl = "http://mts.wibro.agh.edu.pl/~s292511/php/API";

  constructor(
    private http: HttpClient
  ) { }

  /*
    Authentication
  */
  login(login: string, password: string): Observable<User[]> | Observable<Message> {
    const url = "/Auth/login.php";
    return this.http.post<User[]>(this._baseUrl+url, {login: login, password: password});
  }

  /*
    Users
  */
  getAllUsers(): Observable<User[]> {
    const url = "/Users/getAll.php";
    return this.http.get<User[]>(this._baseUrl+url);
  }

  getUserById(id: number): Observable<User> {
    const url = "/Users/getById.php";
    return this.http.post<User>(this._baseUrl+url, {id: id});
  }

  updateUser(user: User): Observable<Message> {
    const url = "/Users/update.php";
    return this.http.post<Message>(this._baseUrl+url, user);
  }

  createUser(user: User): Observable<Message> {
    const url = "/Users/create.php";
    return this.http.post<Message>(this._baseUrl+url, user);
  }

  /*
    Cars
  */
  getAllCars(): Observable<Car[]> {
    const url = "/Cars/getAll.php";
    return this.http.get<Car[]>(this._baseUrl+url);
  }

  getCarById(id: number): Observable<Car> {
    const url = "/Cars/getById.php";
    return this.http.post<Car>(this._baseUrl+url, {id: id});
  }

  updateCar(car: Car): Observable<Message> {
    const url = "/Cars/update.php";
    return this.http.post<Message>(this._baseUrl+url, car);
  }

  createCar(car: Car): Observable<Message> {
    const url = "/Cars/create.php";
    return this.http.post<Message>(this._baseUrl+url, car);
  }

  /*
    Leases
  */
  getAllLeases(): Observable<Lease[]> {
    const url = "/Leases/getAll.php";
    return this.http.get<Lease[]>(this._baseUrl+url);
  }

  getLeaseById(id: number): Observable<Lease> {
    const url = "/Leases/getById.php";
    return this.http.post<Lease>(this._baseUrl+url, {id: id});
  }

  updateLease(lease: Lease): Observable<Message> {
    const url = "/Leases/update.php";
    return this.http.post<Message>(this._baseUrl+url, lease);
  }

  createLease(lease: Lease): Observable<Message> {
    const url = "/Leases/create.php";
    return this.http.post<Message>(this._baseUrl+url, lease);
  }

  /*
    Tasks
  */
  getAllTasks(): Observable<Task[]> {
    const url = "/Tasks/getAll.php";
    return this.http.get<Task[]>(this._baseUrl+url);
  }

  getTaskById(id: number): Observable<Task> {
    const url = "/Tasks/getById.php";
    return this.http.post<Task>(this._baseUrl+url, {id: id});
  }

  updateTask(task: Task): Observable<Message> {
    const url = "/Tasks/update.php";
    return this.http.post<Message>(url, task);
  }

  createTask(task: Task): Observable<Message> {
    const url = "/Tasks/create.php";
    return this.http.post<Message>(url, task);
  }

  /*
    Users
  */
  getAllClients(): Observable<Client[]> {
    const url = "/Clients/getAll.php";
    return this.http.get<Client[]>(this._baseUrl+url);
  }

  getClientById(id: number): Observable<Client> {
    const url = "/Clients/getById.php";
    return this.http.post<Client>(this._baseUrl+url, {id: id});
  }

  updateClient(user: Client): Observable<Message> {
    const url = "/Clients/update.php";
    return this.http.post<Message>(this._baseUrl+url, user);
  }

  createClient(user: Client): Observable<Message> {
    const url = "/Clients/create.php";
    return this.http.post<Message>(this._baseUrl+url, user);
  }

  /*
    Documents
  */
  getDocumentsInfo(): Observable<Document[]> {
    const url = "/Documents/getInfo.php";
    return this.http.get<Document[]>(this._baseUrl+url);
  }

  getDocumentDetails(id: number): Observable<Blob> {
    const url = "/Documents/getDetails.php";
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    return this.http.post<Blob>(this._baseUrl+url, {id: id}, {headers: headers, responseType: 'blob' as 'json' });
  }

  /*
    Posts
  */
  createPost(post: Post): Observable<Message> {
    const url = "/Posts/create.php";
    return this.http.post<Message>(this._baseUrl + url, post);
  }

  editPost(post: Post): Observable<Message> {
    const url = "/Posts/update.php";
    return this.http.put<Message>(this._baseUrl + url, post);
  }

  getPostById(id: number): Observable<Post> {
    const url = "/Posts/getById.php/" + id;
    return this.http.get<Post>(this._baseUrl + url);
  }

  getAllPosts(): Observable<Post[]> {
    const url = "/Posts/getAll.php";
    return this.http.get<Post[]>(this._baseUrl + url);
  }

  deletePost(id: number): Observable<Message> {
    const url = "/Posts/delete.php/" + id;
    return this.http.delete<Message>(this._baseUrl + url);
  }
}
