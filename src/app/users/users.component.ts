import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { AgGridAngular } from 'ag-grid-angular';
import { Router } from '@angular/router';
import { UserRole } from '../models/api.enums';
import { RepositoryService } from '../repository.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  message: string = null

  // grid properties
  columnDefs = [
    { headerName: 'ID', field: 'id', sortable: true, filter: true, checkboxSelection: true },
    { headerName: 'Name', field: 'name', sortable: true, filter: true },
    { headerName: 'Login', field: 'login', sortable: true, filter: true },
    { headerName: 'Email', field: 'email', sortable: true, filter: true },
    { headerName: 'Role', field: 'role', sortable: true, filter: true },
    { headerName: 'Department', field: 'department', sortable: true, filter: true }
  ];

  rowData = Array();

  @ViewChild('usersGrid', { static: false })
  usersGrid: AgGridAngular;

  constructor(
    private apiService: ApiService,
    private router: Router,
    private repository: RepositoryService
  ) {}

  ngOnInit(): void {
    this.apiService.getAllUsers().subscribe(users => {
      const userData = [];
      users.forEach(user => {
        userData.push({
          'id': user.id,
          'name': user.name,
          'login': user.login,
          'email': user.email,
          'role': UserRole[user.role],
          'department': user.department
        })
        this.rowData = userData
      }, err => {
        this.message = err;
      })
    });
  }

  displaySelected() {
    var selectedRows = this.usersGrid.api.getSelectedRows();
    var user = selectedRows[0];

    this.repository.storePath('main/users-form/'+user.id);
    this.router.navigate(['main/users-form', user.id]);
  }

  createNew() {
    this.repository.storePath('main/users-form/0');
    this.router.navigate(['main/users-form', 0]);
  }

}
