import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RepositoryService } from 'src/app/repository.service';
import { StandardErrors } from 'src/app/models/api.enums';
import { User } from 'src/app/models/api.models';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss']
})
export class UsersFormComponent implements OnInit {

  userId: number;
  user = new User()

  userForm = new FormGroup({
    name: new FormControl(''),
    login: new FormControl(''),
    password: new FormControl(''),
    email: new FormControl(''),
    role: new FormControl(''),
    department: new FormControl('')
  });

  message: string = null;
  buttonFunction: string = null;
  title: string = null;
  edit: boolean;

  constructor(
    private api: ApiService,
    private router: Router,
    private repository: RepositoryService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    var id = +this.route.snapshot.paramMap.get("id");
    this.userId = id;

    if (id == 0) {
      // new user
      this.edit = false;
      this.title = 'Create a new user';
      this.buttonFunction = 'CREATE';

    } else {
      // existing user
      this.edit = true;
      this.title = 'Edit user - id: ' + id;
      this.buttonFunction = 'EDIT';

      this.api.getUserById(id).subscribe(user => {
        this.user = user;
        this.prepopulateForm();
      }, err => {
        this.message = StandardErrors.network;
      });
    }
  }

  prepopulateForm() {
    this.userForm.controls['name'].patchValue(this.user.name);
    this.userForm.controls['login'].patchValue(this.user.login);
    this.userForm.controls['password'].patchValue(this.user.password);
    this.userForm.controls['email'].patchValue(this.user.email);
    this.userForm.controls['role'].patchValue(this.user.role);
    this.userForm.controls['department'].patchValue(this.user.department);
  }

  onSubmit() {
    this.message = null;

    var name = this.userForm.value['name'];
    var login = this.userForm.value['login'];
    var password = this.userForm.value['password'];
    var email = this.userForm.value['email'];
    var role = +this.userForm.value['role'];
    var department = this.userForm.value['department'];

    // validate
    if (name.length < 3 || name.length > 20
        || login.length < 3 || login.length > 20
        || password.length < 3 || password.length > 20
        || email.length < 3 ) {

      this.message = StandardErrors.form;
      return;
    }

    this.user.id = this.userId;
    this.user.name = name;
    this.user.login = login;
    this.user.password = password;
    this.user.email = email;
    this.user.role = role;
    this.user.department = department;

    console.log(this.user);

    if (this.edit) {
      this.api.updateUser(this.user).subscribe(res => {
        if (res.message) {
          this.message = res.message;
        }
      }, err => {
          this.message = StandardErrors.network;
      });
    } else {
      this.api.createUser(this.user).subscribe(res => {
        if (res.message) {
          this.message = res.message;
        }
      }, err => {
          this.message = StandardErrors.network;
      });
    }
  }

  onBack() {
    this.repository.storePath('main/users');
    this.router.navigate(['main/users']);
  }

}
