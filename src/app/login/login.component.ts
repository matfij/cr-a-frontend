import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { RepositoryService } from '../repository.service';
import { StandardErrors } from '../models/api.enums';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  authForm = new FormGroup({
    login: new FormControl(''),
    password: new FormControl('')
  });
  message: String = null

  constructor(
    private api: ApiService,
    private router: Router,
    private repository: RepositoryService
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.message = null;

    var login = this.authForm.value['login'];
    var password = this.authForm.value['password'];

    this.api.login(login, password).source.subscribe(res => {
      const data = res.body;
      if (data.message) {
        this.message = data.message;
      } else {
        this.repository.login(data);
        this.router.navigate(["/main"]);
      }
    }, err => {
        this.message = StandardErrors.network;
    });
  }

}
