import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { CarStatus } from '../models/api.enums';
import { RepositoryService } from '../repository.service';

@Component({
  selector: 'app-fleet',
  templateUrl: './fleet.component.html',
  styleUrls: ['./fleet.component.scss']
})
export class FleetComponent implements OnInit {

  message: string = null

  // grid properties
  columnDefs = [
    { headerName: 'ID', field: 'id', sortable: true, filter: true, checkboxSelection: true },
    { headerName: 'Registration number', field: 'registrationNumber', sortable: true, filter: true },
    { headerName: 'Brand', field: 'brand', sortable: true, filter: true },
    { headerName: 'Model', field: 'model', sortable: true, filter: true },
    { headerName: 'ACRISS', field: 'acriss', sortable: true, filter: true },
    { headerName: 'Tires', field: 'tires', sortable: true, filter: true },
    { headerName: 'VIN', field: 'vin', sortable: true, filter: true },
    { headerName: 'Production year', field: 'productionYear', sortable: true, filter: true },
    { headerName: 'Status', field: 'status', sortable: true, filter: true }
  ];

  rowData = Array();

  @ViewChild('fleetGrid', { static: false })
  fleetGrid: AgGridAngular;

  constructor(
    private repository: RepositoryService,
    private apiService: ApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.apiService.getAllCars().subscribe(cars => {
      const carData = [];
      cars.forEach(car => {
        carData.push({
          'id': car.id,
          'department': car.department,
          'registrationNumber': car.registrationNumber,
          'brand': car.brand,
          'model': car.model,
          'acriss': car.acriss,
          'tires': car.tires,
          'vin': car.vin,
          'productionYear': car.productionYear,
          'status': CarStatus[car.status]
        })
        this.rowData = carData;
      }, err => {
        this.message = err;
      })
    });
  }

  displaySelected() {
    var selectedRows = this.fleetGrid.api.getSelectedRows();
    var user = selectedRows[0];

    this.repository.storePath('main/fleet-form/'+user.id);
    this.router.navigate(['main/fleet-form', user.id]);
  }

  createNew() {
    this.repository.storePath('main/fleet-form/0');
    this.router.navigate(['main/fleet-form', 0]);
  }
}
