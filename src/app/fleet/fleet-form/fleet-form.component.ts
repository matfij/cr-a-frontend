import { Component, OnInit } from '@angular/core';
import { Car } from 'src/app/models/api.models';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RepositoryService } from 'src/app/repository.service';
import { StandardErrors } from 'src/app/models/api.enums';

@Component({
  selector: 'app-fleet-form',
  templateUrl: './fleet-form.component.html',
  styleUrls: ['./fleet-form.component.scss']
})
export class FleetFormComponent implements OnInit {

  carId: number;
  car = new Car()

  fleetForm = new FormGroup({
    department: new FormControl(''),
    registrationNumber: new FormControl(''),
    brand: new FormControl(''),
    model: new FormControl(''),
    acriss: new FormControl(''),
    tires: new FormControl(''),
    vin: new FormControl(''),
    productionYear: new FormControl(''),
    status: new FormControl('')
  });

  message: string = null;
  buttonFunction: string = null;
  title: string = null;
  edit: boolean;

  constructor(
    private api: ApiService,
    private router: Router,
    private repository: RepositoryService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    var id = +this.route.snapshot.paramMap.get("id");
    this.carId = id;

    if (id == 0) {
      // new car
      this.edit = false;
      this.title = 'Create a new car';
      this.buttonFunction = 'CREATE';

    } else {
      // existing car
      this.edit = true;
      this.title = 'Edit car - id: ' + id;
      this.buttonFunction = 'EDIT';

      this.api.getCarById(id).subscribe(car => {
        this.car = car;
        this.prepopulateForm();
      }, err => {
        this.message = StandardErrors.network;
      });
    }
  }

  prepopulateForm() {
    this.fleetForm.controls['department'].patchValue(this.car.department);
    this.fleetForm.controls['registrationNumber'].patchValue(this.car.registrationNumber);
    this.fleetForm.controls['brand'].patchValue(this.car.brand);
    this.fleetForm.controls['model'].patchValue(this.car.model);
    this.fleetForm.controls['acriss'].patchValue(this.car.acriss);
    this.fleetForm.controls['tires'].patchValue(this.car.tires);
    this.fleetForm.controls['vin'].patchValue(this.car.vin);
    this.fleetForm.controls['productionYear'].patchValue(this.car.productionYear);
    this.fleetForm.controls['status'].patchValue(this.car.status);
  }

  onSubmit() {
    this.message = null;

    var department = this.fleetForm.value['department'];
    var registrationNumber = this.fleetForm.value['registrationNumber'];
    var brand = this.fleetForm.value['brand'];
    var model = this.fleetForm.value['model'];
    var acriss = this.fleetForm.value['acriss'];
    var tires = this.fleetForm.value['tires'];
    var vin = this.fleetForm.value['vin'];
    var productionYear = this.fleetForm.value['productionYear'];
    var status = this.fleetForm.value['status'];

    // validate
    if (department.length < 3 || department.length > 20
        || registrationNumber.length < 3 || registrationNumber.length > 20
        || brand.length < 3 || brand.length > 20
        || model.length < 3 ) {

      this.message = StandardErrors.form;
      return;
    }

    this.car.id = this.carId;
    this.car.department = department;
    this.car.registrationNumber = registrationNumber;
    this.car.brand = brand;
    this.car.model = model;
    this.car.acriss = acriss;
    this.car.tires = tires;
    this.car.vin = vin;
    this.car.productionYear = productionYear;
    this.car.status = status;

    if (this.edit) {
      this.api.updateCar(this.car).subscribe(res => {
        if (res.message) {
          this.message = res.message;
        }
      }, err => {
          this.message = StandardErrors.network;
      });
    } else {
      this.api.createCar(this.car).subscribe(res => {
        if (res.message) {
          this.message = res.message;
        }
      }, err => {
          this.message = StandardErrors.network;
      });
    }
  }

  onBack() {
    this.repository.storePath('main/fleet');
    this.router.navigate(['main/fleet']);
  }

}
