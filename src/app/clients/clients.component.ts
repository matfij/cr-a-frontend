import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { UserRole } from '../models/api.enums';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  errorMessage: string = null

  // grid properties
  columnDefs = [
    { headerName: 'ID', field: 'id', sortable: true, filter: true, checkboxSelection: true },
    { headerName: 'Name', field: 'name', sortable: true, filter: true },
    { headerName: 'Surname', field: 'surname', sortable: true, filter: true },
    { headerName: 'Age', field: 'age', sortable: true, filter: true },
    { headerName: 'Pesel', field: 'pesel', sortable: true, filter: true },
    { headerName: 'Address', field: 'address', sortable: true, filter: true },
    { headerName: 'Email', field: 'email', sortable: true, filter: true },
    { headerName: 'Phone', field: 'phone', sortable: true, filter: true },
  ];

  rowData = Array();

  @ViewChild('usersGrid', { static: false })
  usersGrid: AgGridAngular;

  constructor(
    private apiService: ApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.apiService.getAllClients().subscribe(clients => {
      const clientData = [];
      clients.forEach(client => {
        clientData.push({
          'id': client.id,
          'name': client.name,
          'surname': client.surname,
          'age': client.age,
          'pesel': client.pesel,
          'address': client.address,
          'email': client.email,
          'phone': client.phone
        })
        this.rowData = clientData
      }, err => {
        this.errorMessage = err;
      })
    });
  }

  displaySelected() {
    var selectedRows = this.usersGrid.api.getSelectedRows();
    var user = selectedRows[0];

    this.router.navigate(['main/users-form', user.id]);
  }

  createNew() {
    this.router.navigate(['main/users-form', 0]);
  }

}
