import { OnInit, Component, ViewChild, Output, Input, ɵCompiler_compileModuleAndAllComponentsSync__POST_R3__, OnDestroy, EventEmitter } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { PostCategories } from '../../models/api.enums';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'src/app/api.service';
import { Post } from 'src/app/models/api.models';
import { Subscription } from 'rxjs';
import { RepositoryService } from 'src/app/repository.service';

@Component({
    selector: 'app-forum-post',
    templateUrl: './forum-post.component.html',
    styleUrls: ['./forum-post.component.scss']
})
export class ForumPostComponent implements OnInit, OnDestroy {
    @Input() name;
    @Input() submitButtonText;
    @Input() id;
    listReload = new EventEmitter();
    postForm: FormGroup;
    postCategories = ['warning', 'info'];
    loading = false;
    subscitions: Subscription[] = [];

    constructor(
      public activeModal: NgbActiveModal,
      private formBuilder: FormBuilder,
      private apiService: ApiService,
      private repository: RepositoryService
    ) {}

    ngOnInit() {
      this.postForm = this.formBuilder.group({postTitle: [''],
        postMessage: [''],
        postCategory: [''],
      });
    }

    ngOnDestroy(): void {
        this.subscitions.forEach(s => s.unsubscribe());
    }

    submit() {
      switch (this.submitButtonText) {
        case 'Create post':
          this.createPost();
          break;
        case 'Edit post':
          this.editPost();
          break;
        case '':
          this.getPostById();
          break;
      }
    }

    getPostById() {}

    editPost() {}

    private createPost() {
      this.loading = true;

      const title = this.postForm.get('postTitle').value;
      const message = this.postForm.get('postMessage').value;
      const category = this.postForm.get('postCategory').value;

      const newPost: Post = {
        title: title,
        authorId: this.repository.getUserId(),
        category: category,
        message: message
      };

      this.subscitions.push(this.apiService.createPost(newPost).subscribe(m => {
        this.loading = false;
        this.listReload.emit(null);
      }, e => {
        console.error(e);
        this.loading = false;
        this.activeModal.close();
      }));
    }
}
