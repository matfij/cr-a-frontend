import { OnInit, Component, ViewChild, OnDestroy } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { PostCategories } from '../../models/api.enums';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ForumPostComponent } from '../forum-post/forum-post.component';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/api.service';
import { switchMap } from 'rxjs/operators';
import { User } from 'src/app/models/api.models';

@Component({
    selector: 'app-forum-summary',
    templateUrl: './forum-summary.component.html',
    styleUrls: ['./forum-summary.component.scss']
})
export class ForumSummaryComponent implements OnInit, OnDestroy {

    // grid properties
    columnDefs = [
      { headerName: 'ID', field: 'id', sortable: true, width: 80 },
      { headerName: 'Category', field: 'category', sortable: true, width: 80 },
      { headerName: 'Author', field: 'author', sortable: true, width: 80 },
      { headerName: 'Title', field: 'title', sortable: true, width: 80 },
      { headerName: 'Message', field: 'message', sortable: true, width: 900 },
    ];

    rowData = Array();

    rowIsSelected = false;
    subscitions: Subscription[] = [];

    @ViewChild('forumSummaryGrid', { static: false })
    forumSummaryGrid: AgGridAngular;

    loading = false;
    allUsers = new Map<number, string>();

    constructor(
      private modalService: NgbModal,
      private apiService: ApiService
    ) {}

    ngOnInit(): void {
      this.reloadList();
    }

    private reloadList() {
      this.subscitions.push(this.apiService.getAllUsers().pipe(switchMap(allUsers => {
        allUsers.forEach(u => this.allUsers.set(u.id, u.login));
        return this.apiService.getAllPosts();
      })).subscribe(posts => {
        const tempRowData = [];
        posts.forEach(p => {
          const login = this.allUsers.get(p.authorId);
          let category = 'warning';
          if (p.category == 1) {
            category = 'info';
          }
          tempRowData.push({ id: p.id, author: login, title: p.title, category: category, message: p.message });
          this.rowData = tempRowData;
          console.log(this.rowData)
        });
      }));
    }

    ngOnDestroy(): void {
      this.subscitions.forEach(s => s.unsubscribe());
    }

    openAddPost() {
      const modalRef = this.modalService.open(ForumPostComponent);
      modalRef.componentInstance.name = 'Post creation';
      modalRef.componentInstance.submitButtonText = 'Create post';

      this.subscitions.push(modalRef.componentInstance['listReload'].subscribe(() => this.reloadList()));
    }

    openShowPost() {
      const modalRef = this.modalService.open(ForumPostComponent);
      modalRef.componentInstance.name = 'Post detials';
      modalRef.componentInstance.submitButtonText = '';

      const selectedNodes = this.forumSummaryGrid.api.getSelectedNodes();
      const id = selectedNodes[0].data.id;
      modalRef.componentInstance.postId = id;
    }

    openEditPost() {
      const modalRef = this.modalService.open(ForumPostComponent);
      modalRef.componentInstance.name = 'Post edition';
      modalRef.componentInstance.submitButtonText = 'Edit post';

      this.subscitions.push(modalRef.componentInstance['listReload'].subscribe(() => this.reloadList()));
    }

    deletePost() {
      const selectedNodes = this.forumSummaryGrid.api.getSelectedNodes();
      const id = selectedNodes[0].data.id;
      this.subscitions.push(this.apiService.deletePost(id).subscribe(m => {
          this.loading = false;
          this.reloadList();
      }, e => {
          console.error(e);
          this.loading = false;
      }));
    }

}
