import { Injectable } from '@angular/core';
import { User } from './models/api.models';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  // app state
  private _userId: number;
  private _user: User;
  private _path: string;

  constructor(
    private api: ApiService
  ) {}

  start() {
    this.api.getUserById(this._userId).subscribe(user => {
      this._user = user;
    });
  }


  login(user: User) {
    this._user = user;
    localStorage.setItem('id', user.id.toString());
    this.storePath('main/dashboard');
  }

  logout() {
    localStorage.clear();
    this._user = null;
    this._path = null;
    this._userId = null;
  }

  isLogin(): boolean {
    if (this._userId = +localStorage.getItem('id'))  return true
    else  return false
  }

  get user(): User {
    return this._user;
  }

  set user(user: User) {
    this._user = user;
  }

  storePath(path: string) {
    this._path = path;
    localStorage.setItem('path', this._path);
  }

  getPath() {
    this._path = localStorage.getItem('path');
    return this._path;
  }

  getUserId() {
    this._userId = +localStorage.getItem('id');
    return this._userId;
  }
}
