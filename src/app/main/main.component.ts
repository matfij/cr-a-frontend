import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RepositoryService } from '../repository.service';
import { User } from '../models/api.models';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  user = new User();

  constructor(
    private router: Router,
    private repository: RepositoryService,
    private api: ApiService
  ) { }

  ngOnInit(): void {
    // manage user
    const id = this.repository.getUserId();
    this.api.getUserById(id).subscribe(user => {
      this.user = user;
    });

    // manage path
    const path = this.repository.getPath();
    this.router.navigate([path]);
  }

  loadComponent(component: number) {
    switch(component) {
      case 1: { this.repository.storePath('main/dashboard'); this.router.navigate(['main/dashboard']); break; }
      case 2: { this.repository.storePath('main/users'); this.router.navigate(['main/users']); break; }
      case 3: { this.repository.storePath('main/fleet'); this.router.navigate(['main/fleet']); break; }
      case 4: { this.repository.storePath('main/leases'); this.router.navigate(['main/leases']); break; }
      case 5: { this.repository.storePath('main/tasks'); this.router.navigate(['main/tasks']); break; }
      case 6: { this.repository.storePath('main/clients'); this.router.navigate(['main/clients']); break; }
      case 7: { this.repository.storePath('main/documents'); this.router.navigate(['main/documents']); break; }
    }
  }

  logout() {
    this.repository.logout();
    this.router.navigate(['login']);
  }
}
