import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { DocumentsComponent } from './documents/documents.component';
import { FleetComponent } from './fleet/fleet.component';
import { LeasesComponent } from './leases/leases.component';
import { TasksComponent } from './tasks/tasks.component';
import { ClientsComponent } from './clients/clients.component';
import { FleetFormComponent } from './fleet/fleet-form/fleet-form.component';


const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'main', component: MainComponent, canActivate: [AuthGuard],
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'users', component: UsersComponent },
      { path: 'fleet', component: FleetComponent },
      { path: 'leases', component: LeasesComponent },
      { path: 'tasks', component: TasksComponent },
      { path: 'clients', component: ClientsComponent },
      { path: 'documents', component: DocumentsComponent },
      { path: 'users-form/:id', component: UsersFormComponent },
      { path: 'fleet-form/:id', component: FleetFormComponent },
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
