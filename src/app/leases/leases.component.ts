import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { CarStatus, LeaseStatus } from '../models/api.enums';

@Component({
  selector: 'app-leases',
  templateUrl: './leases.component.html',
  styleUrls: ['./leases.component.scss']
})
export class LeasesComponent implements OnInit {

  message: string = null

  // grid properties
  columnDefs = [
    { headerName: 'ID', field: 'id', sortable: true, filter: true, checkboxSelection: true },
    { headerName: 'Client ID', field: 'clientId', sortable: true, filter: true },
    { headerName: 'Car ID', field: 'carId', sortable: true, filter: true },
    { headerName: 'Start date', field: 'startDate', sortable: true, filter: true },
    { headerName: 'End date', field: 'endDate', sortable: true, filter: true },
    { headerName: 'Status', field: 'status', sortable: true, filter: true },
    { headerName: 'Department', field: 'department', sortable: true, filter: true }
  ];

  rowData = Array();

  @ViewChild('fleetGrid', { static: false })
  fleetGrid: AgGridAngular;

  constructor(
    private apiService: ApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.apiService.getAllLeases().subscribe(leases => {
      const leaseData = [];
      leases.forEach(lease => {
        leaseData.push({
          'id': lease.id,
          'clientId': lease.clientId,
          'carId': lease.carId,
          'startDate': lease.startDate,
          'endDate': lease.endDate,
          'status': LeaseStatus[lease.status],
          'department': lease.department,
        })
        this.rowData = leaseData;
      }, err => {
        this.message = err;
      })
    });
  }

  displaySelected() {
    var selectedRows = this.fleetGrid.api.getSelectedRows();
    var user = selectedRows[0];

    // this.router.navigate(['main/users-form', user.id]);
  }

  createNew() {
    // this.router.navigate(['main/users-form', 0]);
  }

}
